# storage/nvdimm/fsdax_dm_log_writes

Storage: nvdimm fsdax dm_log_writes test, BZ1539264

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
